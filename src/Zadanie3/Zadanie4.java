package Zadanie3;

public class Zadanie4 {
    public static void main(String[] args) {
        boolean jest_cieplo = false;
        boolean wieje_wiatr = true;
        boolean swieci_slonce = false;

        boolean ubieram_sie_cieplo = false;
        if (wieje_wiatr || !jest_cieplo) {
            ubieram_sie_cieplo = true;
        }
        System.out.println("ubieram sie cieplo = " + ubieram_sie_cieplo);

        boolean biore_parasol = true;
        if (!swieci_slonce || !wieje_wiatr) {
            biore_parasol = true;
        }
        System.out.println("biore parasol = " + biore_parasol);

        boolean ubieram_kurtke = true;
        if (!swieci_slonce && !jest_cieplo && wieje_wiatr) {
            ubieram_kurtke = true;
        }
        System.out.println("ubieram_kurtke =" + ubieram_kurtke);

    }
}
