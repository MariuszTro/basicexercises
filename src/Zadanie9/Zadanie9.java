package Zadanie9;

import java.util.Scanner;

public class Zadanie9 {
    public static void main(String[] args) {
    Scanner konsola = new Scanner(System.in);

        System.out.println("Podaj wage: ");
        int waga = konsola.nextInt();
        System.out.println("Podaj wzrost: ");
        int wzrost = konsola.nextInt();
        System.out.println("Podaj wiek: ");
        int wiek = konsola.nextInt();

        if (waga >= 180) {
            System.out.println("nie wejdziesz => waga");
        } else if (wiek <= 10 && wiek >= 80) {
            System.out.println("nie wejdziesz => wiek");
        } else if (wzrost < 150 || wzrost > 220) {
            System.out.println("nie mozesz wejsc => wzrost");
        } else {
            System.out.println("mozesz wejsc");
        }

    }
}