package Zadanie18;

import java.util.Scanner;

public class Zadanie18 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ilosc liczb:");
        int iloscLiczb = scanner.nextInt();

        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        double sum = 0;
        for (int i = 0; i < iloscLiczb; i++) {
            int liczba = scanner.nextInt();

            if (liczba > max) {
                max = liczba;
            }
            if (liczba < min) {
                min = liczba;
            }
            sum += liczba;
        }
        double srednia = (sum / iloscLiczb);
        System.out.println("Min+max = " + (min + max));
        System.out.println("Srednia = " + srednia);
    }
}

